Restarting and Resuming
=============================

If the simulation stops mid-run in the BO cycle of data acquisitions (e.g. cut off by the HPC queue) it is possible to perform an exact restart by simply using the **boss.rst** file as the new input file. 
::

	> boss op boss.rst

At the start of this run, BOSS will read the full list of previously acquired data, load the latest model hyperparameters, and proceed with the BO cycle until the original *iterpts* value is reached. Note that restarting the simulation in the same directory will overwrite the original **boss.rst** file, but the new one will contain the full data list (as if the calculation had not been interrupted), ready for postprocessing.


**Resuming (Continuing).**
Should the user not be satisfied with the level of BO convergence after the simulation is complete, it is possible to resume the simulation in a manner identical to the restart above. In this case, the user would modify **boss.rst** to increment *iterpts* value, e.g. **boss_mod.rst**.  
::

	> boss op boss_mod.rst

The next run will load all previously acquired data, and continue acquiring new data until the new *iterpts* target is achieved. The resulting **boss.rst** will contain a full list of data for the two runs.

**Reusing a dataset.**
It is possible to re-use an old dataset (or fit a surrogate model to any data) by placing the data list under the keyword *RESULTS* in the BOSS input file. Keyword *initpts* should be set to the number of data lines to be read under the *RESULTS* keyword. See :download:`Tutorial 3 <../tutorials/cli/tutorial_3.zip>` for this use case. Should the list of data contain only X (states) and no Y (label), BOSS will iterate through the list to evaluate Y and then fit the model.

