.. _resuming_py:

Resuming
=============
Note: you can also download this tutorial as a :download:`python script <pyfiles/resuming.py>` or a :download:`notebook <notebooks/resuming.ipynb>`.

If we run BOSS and find that the results are not satisfactory, the optimization
can easily be resumed as long as the original BOMain object has not gone out of scope.

To illustrate this, we consider a 2D function, which has an approximate global minimum :math:`y = 1.463` at :math:`x = (-4.000, -3.551)`.

.. code-block:: python

    from boss.bo.bo_main import BOMain

    def func_2d(X):
        x = X[0, 0]
        y = X[0, 1]
        z = 0.01 * ((x ** 2 + y - 11) ** 2 + (x + y ** 2 - 7) ** 2 + 20 * (x + y))
        return z

    bo = BOMain(
        func_2d,
        bounds=[[-5.0, 5.0], [-5.0, 5.0]],
        yrange=[0.0, 10.0],
        initpts=5,
        iterpts=10
    )
    res = bo.run()
    print('Predicted global min: {} at x = {}'.format(res.fmin, res.xmin))

Here, we only did 10 BO iterations, which is on the low side for a 2D optimization problem. We can improve on our result by resuming our optimization and adding, say 20 additional iterations. To do so, we invoke the ``BOMain.resume`` method
and specify to new total number of iterations = 10 + 20 = 30:

.. code-block:: python

    res = bo.resume(iterpts=30)
    print('Predicted global min after resuming: {} at x = {}'.format(res.fmin, res.xmin))
