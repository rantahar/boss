Keywords
++++++++
The behaviour of BOSS can be controlled through a set of global keywords that are valid in
both the Python and command line interfaces. All the possible keywords and their values are described below grouped
by theme in the following subsections. In the command line interface, keywords are supplied via the input file as line-separated
``keyword`` ``value`` pairs. Any text preceded by a Hashtag ``#`` in the input file is interpreted as a comment and will
be ignored by BOSS.

File Handling
-------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``userfn``\ : filepath [name] (**No default**)
| A relative or absolute path to a python script containing a function (called *name*, or *f* if no name is given), that returns the
  objective value :math:`f(\mathbf{x})` given a variable array :math:`\mathbf{x}`.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``outfile``\ : filepath (boss.out)
| Filepath to the main output file which contains a summary of the optimization.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``rstfile``\ : filepath (boss.rst)
| Filepath to the restart file which contains the current keywords as well as acquired
  data and model hyperparameters. Can be used as an input file for
  restarting a previous optimization and/or doing post-processing.

Main Options
------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``bounds``\ : min1 max1; min2 max2; ... minN maxN (**No default**)
| Bounds for the variables defining the domain/space of the user
  function (see ``userfn``). The user function will only be passed arrays
  :math:`\mathbf{x}` that belong to this domain/space.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``kernel``\ : rbf, stdp, mat32 or mat52 (rbf)
| Which kernel (i.e. covariance function) to use. If just one keyword is
  specified, that kernel will be used for all variables. Mixing kernels
  for N different variables (by multiplication) is done by specifying
  N kernels. For instance, a 3D problem where the last variable is not
  periodic could be specified with stdp stdp rbf.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``periods``\ : per1 per2 ... perN (bounds[max] - bounds[min] for each variable)
| The periodicity of each variable, required by the stdp kernel.
  Defaults to the interval length of the individual variable bounds.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``yrange``\ : y_min y_max (-10.0 10.0)
| An estimated range for the objective function, i.e. lower and upper limits that objective values :math:`f(\mathbf{x})`
  are likely to fall within for any given :math:`\mathbf{x}` in the domain.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``ynorm``\ : 1 or 0 (0)
| Turns normalization of y-data on or off. If turned on, the ``yrange``
  keyword does not need to be specified.


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``noise``\ : non-negative float (:raw-html:`10<sup>-12</sup>`)
| Estimated variance for the (Gaussian) noise in the objective function :math:`f(\mathbf{x})` evaluations.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``inittype``\ : sobol, random or grid (sobol)
| The method for creating the initial sampling locations
  :math:`{\bf x}`. If the input file is a restart file, any data points
  supplied therein will override this setting.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``initpts``\ : positive integer (5)
| The number of initial data points to create/read. If the input file is
  a restart file, data points supplied therein will override this setting.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``iterpts``\ : positive integer (:raw-html:`int(15*dim<sup>1.5</sup>)`)
| The number of Bayesian optimization iterations to run.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``gm_tol``\ : dxtol niters (None)
|  Turns on a convergence check where the BO stops if the change in the global minimum prediction
  location :math:`\mathbf{\hat{x}}` has not exceeded ``dxtol`` in the last ``niters`` BO iterations (even if the 
  the current number of iterations is less than ``iterpts``). The default value of None means that no convergence check is performed.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``verbosity``\ : 0, 1 or 2 (1)
| The level of verbosity in the BOSS main output file. The higher the
  level, the more details are printed out in main output file.

Data Acquisitions
-----------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqfn``\ : elcb, lcb, explore or exploit (elcb)
| The choice of the acquisition function for selecting the next sampling
  location(s) during the BO process. Note that if the predictive variance at the acquisition location 
  is smaller than ``acqtol``, that acquisition is discarded and a new location is sampled using pure exploration.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqfnpars``\ : par1, par2, ... (No default)
| Additional arguments to the acquisition function. Currently only used
  (optionally) by LCB to give a tradeoff parameter.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``acqtol``\ : float or None (0.001)
| The tolerance for how small of a model variance is allowed in the next
  suggested acquisition location :math:`x_{next}` before triggering pure
  exploration.

GP Hyperparameters
------------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetainit``: var ls1 ls2 .. lsN
| (var :math:`\Delta`\yrange/2 or 1/2 if ``ynorm``\=1; ls :math:`\pi`/10 for periodic,
  (bounds[max] - bounds[min])/20 for non-periodic
  variables)
| Initial value of kernel variance, followed by initial values for the
  kernel lengthscales.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetabounds``\ : minv maxv; min1 max1; min2 max2; ... minN maxN
| (``thetainit``/1000  ``thetainit``\*1000 for
  variance; ``thetainit``/100  ``thetainit``\*100 for
  each lengthscale)
| Hard constraints for kernel variance and lengthscales.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetaprior``\ : gamma or None (gamma)
| Prior distribution to use for the hyperparameters. If set to none, no
  informative prior is used. Gamma distribution takes two parameters:
  shape and rate. (See ``thetapriorpar``)

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``thetapriorpar``\ :
  par1_v par2_v; par1_1 par2_1; par1_2 par2_2; ... par1_N par2_N
| (defaults based on ``yrange`` and ``kernel``)
| Parameters of the prior for kernel variance and lengthscales.
  Parameters required depend on the prior (see ``thetaprior``).

Hyperparameter Optimization
---------------------------

| **keyword: choices (default)**
| **description**

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``initupdate``\ : 1 or 0 (1)
| Whether to optimize the kernel hyperparameters immediately
  initializing the first GP model containing the initial data.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updatefreq``\ : positive integer (1)
| The frequency (in terms of BO iterations) of optimizing the
  hyperparameters by maximizing the marginal likelihood.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updateoffset``\ : non-negative integer (0)
| The BO iteration from which to start optimizing kernel hyperparameters
  in intervals (see ``updatefreq``).

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``updaterestarts``\ : positive integer (2)
| How many random restarts to do in the kernel hyperparameter’s space
  when optimzing their values (i.e. maximizing marginal likelihood).

.. _keywords_pp:

Post-processing
---------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_iters``\ : ``it1 it2 it3 ... itN`` (all from 0 to
  ``iterpts``)
| The iterations to apply all post-processing functionality to. The 0th
  iteration refers to the first fit of GP model including just the
  initial data.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_models``\ : 1 or 0 (0)
| Whether to output and plot GP models and uncertainty. See also
  ``pp_model_slice``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_acq_funcs``\ : 1 or 0 (0)
| Whether to output and plot acquisition functions or slices of them in
  a grid according to ``pp_model_slice``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_model_slice``\ : ``var1 var2 npts`` (1 1 50 in 1D; 1 2 25
  in 2D+)
| Which (max 2D) cross-section of the objective function domain to use
  in output and plots. First two integers define the cross-section and
  last determines how many points per edge in the dumped grid. If first
  two are the same, it means a 1D cross-section.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_var_defaults``\ : ``def1 def2 ... defN`` (``None``)
| Which default values to fix each variable to if they are not included
  in the model and true function output and plots. If this keyword is
  ``None`` the :math:`\hat{x}` values for each variable are used as a
  default. For acquisition function plots the same applies, but default
  values are the :math:`x_{next}` values if they are available.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_truef_npts``\ : positive integer (``None``)
| Determines how big of a grid to use for outputting the true/objective
  function by making extra calls to user function. The grid size is
  defined as points per edge. Leaving this keyword to its default None
  will result in no true function dump.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_local_minima``\ : float between 0.0 and 1.0 (``None``)
| Determines the proportion (:math:`x \cdot 100\%`) of the lowest
  acquisitions to use for starting minimizers in an attempt to extract
  all the local minima of the GP model. Leaving this keyword to its
  default None means no local minima search.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``pp_truef_at_xhats``\ : 1 or 0 (0)
| Whether to calculate true function values at model minimum prediction
  locations :math:`\mathbf{\hat{x}}`. Note that this causes extra calls
  to the user function.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``mep_rrtsteps``\ : positive integer (10000)
| The maximum number of steps that the energy threshold is increased and
  the RRT trees grown. Defines the energy threshold step
  :math:`\Delta e` by dividing the energy range.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``mep_maxe``\ : float (defaults to maximum observed energy)
| The high end of the energy range that is considered. Must be higher
  than the maximum energy of all minimum energy paths. (The low end is
  always taken as the energy of the second lowest local minimum.)

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``mep_precision``\ : positive integer (25)
| The precision of collision detection and density of NEB points. Is
  defined as the number of points that would be considered by a path
  directly along the shortest dimension.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``mep_nebsteps``\ : integer, at least 2 (20)
| The maximum number of iterations for optimizing the obtained paths
  with the nudged elastic band method, before returning the final
  result.

Miscellaneous
--------------

| **keyword: choices (default)**
| **description**


.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``seed``\ : non-negative integer (None)
| A fixed seed passed to all random generators invoked by BOSS. By specifying a seed the output of BOSS becomes deterministic, which facilitates, .e.g,  debugging and performance tests.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``parallel_optims``\ : non-negative integer (0)
| The number of parallel optimizers that are launched by BOSS during optimization of the acquisition function and the surrogate model (for determining the global minimum). This option can give significant speed-ups on supercomputers but should not be enabled otherwise. We recommend testing with 12 to 24 parallel optimizers and setting the environment variable ``OMP_NUM_THREADS=1``.

.. raw:: html

   <hr style="margin-top:-20px;margin-bottom:5px">

| ``minfreq``\ : positive integer (1)
| The frequency with which the surrogate models is minimized to determine the current global minimum. Specifically, the surrogate models is minimized every ``minfreq``-th iteration. Increasing ``minfreq`` will thus lead to fewer minimizations and better performance at the cost of missing global minimum information in the output.

