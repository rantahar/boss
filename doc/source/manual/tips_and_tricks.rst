Tips and Tricks
+++++++++++++++

-  **Before starting.** Consider your research question, target
   objective and relevant parameters. Define your variables, consider
   their domain boundaries and relative importance. Whenever possible,
   choose periodic variables. Consider the computational implementation
   and prepare scripts and simulations, especially the user function.

-  **Quick start with BOSS**. Define number of variables and their
   domains. Provide an estimate for f(x) variation. With a new problem, 
   always start with initial tests from a 1D case. 

-  **Initial tests.** 1D tests are highly recommended when starting with
   a new problem. You can test each of your variables separately in 1D,
   while keeping the others fixed to reference values. Compute the true
   function (PP) and check if the model converge to the correct
   function. If not, you should adjust BOSS defaults. If yes, take note
   of final model hyperparameters: you can start N-D runs from these
   values. In the case of  fast acquisitions, the true function check can 
   be done in 2D as well.

-  **Quality check.** The most important quality check of BOSS results is 
   simulation convergence. Convergence of the model (global & local minima)
   and the GP hyperparameters indicate sufficient dataset sizes for optimal
   learning. Oscillations in these values suggests model instability, and we
   recommend continuing the BO iterations until convergence. 
   Key performance indicators (case specific) are:
   How many data points are necessary to i) locate the
   global minimum; ii) converge the surrogate model? How does
   convergence change with increasing dimensions and number of minima?

-  **High-D simulations.** Start with optimal hyperparameters and
   priors. Take symmetry into account wherever possible by: a)
   restricting your period and b) multiplying in no. of datapoints.
   Monitor convergence of both global minimum and surrogate model
   (define markers for chemical reasoning for the latter: then is a
   model correct?).

.. raw:: html

   <h3>Troubleshooting</h3>

* The most common runtime error with BOSS occurs when the user function
  script fails to compute and/or return the label to an acquisition query. 
  This can be identified by correct BOSS initalisation in the boss.out file 
  with the stop occurring at the first ``initpt``, even though python messages
  can be confusing. Please ensure that the external scripts function correctly 
  before plugging them into BOSS.
* BOSS iterations carry the overhead of computing local minimum estimates
  which are needed for convergence tracking, but not for BO. If iterations take
  too long, you can switch this off.
* In the limit of 1000s of data points, BOSS iteration times can be long (>30min). 
  BO acquisition functions are most effective with smaller datasizes; with large
  datasets, iterative acquisitions will take a long time, and a randomly sampled 
  dataset could be used to build surrogate models instead.
