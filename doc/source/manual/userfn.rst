User function
+++++++++++++

The user function serves to supply data points to the active learning
BOSS algorithm, in response to acquisition function queries. It can 
be adapted to any kind of data source, from analytic functions to 
various simulation types.

| Simulation data involves the target function :math:`f(x)` evaluated
  for a set of simulation variables :math:`x`. Data
  acquisition is  managed via the ``user_function`` Python script, which
  evaluates and returns :math:`f(x)`.

.. figure:: ../figures/user_fn.png
   :width: 100.0%

   Fig. 1: Tasks performed by the BO algorithm and the user function.
   Information is exchanged at the interface.

Fig. 1 illustrates how the user function script is
employed. At each iteration :math:`i`, the script receives the feature
vector :math:`x_i`, and returns :math:`f(x_i)`. The operations required
for this typically feature the following steps: a) converting the
:math:`x_i` state vector into the relevant physics quantities; b)
preparing the related atomistic configuration (or other); c) executing
the calculation and d) parsing output files for the target property.
Steps a)-d) can be carried out in Python, Bash, Perl MATLAB or any other
language, because it possible to write :math:`x_i, f(x_i)` to disk. In
the case of Python scripts, we recommend to skip the disk i/o and return
variables directly to the function. The time-limiting step in the entire 
BOSS run is c), which is typically executed in parallel on HPC platforms.

.. raw:: html

   <h2>Degrees of freedom</h2>

BOSS is a tool for global phase space search, looking for optimal solutions
in the domain space of several parameters critical to a physical process.
Search dimensionality is determined by the number of parameters considered,
i.e. degrees of freedom (DoF) in the problem. In theory, this is a very large
number; in practice, there are usually several parameters that
critically affect the target property and these should be selected as
DoF. 

Success of BOSS simulations is guaranteed by a good choice of DoF
variables, system parameters that critically determine the target
property. If the system parameters reflect physical variables, the BOSS
surrogate models will be physically interpretable. A range of choices 
successfully employed in BOSS to date include:

* torsional angles: for molecular conformer search, or flexible molecules;
* molecular orientation, registry (x-y) and height above the surface: for
  molecular surface adsorption studies;
* AI model hyperparameters: for simultaneous optimisation of hyperparameters 
  of materials descriptors and machine learning models;
* theoretical model parameters: for optimising theoretical models to 
  fit experimental data;
* experimental processing conditions: for optimising the outcomes of
  experiments.


.. figure:: ../figures/fig_atmo.png
   :width: 70.0%

   BOSS application to surface adsorption of a molecular conformer:
   orientational degrees of freedom.

.. Success of BOSS simulations is guaranteed by a good choice of DoF
  variables, system parameters that critically determine the target
  property. The stability of molecular conformers typically depends on the
  torsional angles. Molecule-surface interactions (see Fig `4 <#dof>`__)
  depend on molecular orientation and registry (x-y) location, while
  molecule-surface distance typically introduces a constant shift to the
  interaction. Performance of machine learning models depends on a handful
  of key parameters. Accuracy of DFT simulations depends on several
  principal settings. Many properties can be tuned with BOSS, as long as
  the relevant DoFs are correctly identified.

Surrogate model complexity depends on the dimensionality (N), number of
local minima and the spread of their values. BOSS has been tested in
up to 10D with few minima, or up to 6D with many minima. The number of
data acquisitions needed for meaningful results increases with search
complexity, e.g. up to 100 data for 1-3D, and up to 500 for 3-6D.
Convergence behaviour depends on problem details, and on kernel
choice: periodic kernels scale considerably better with dimensions
than non-periodic one.