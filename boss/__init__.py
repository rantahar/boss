__version__ = "1.4"
__url__ = "https://gitlab.com/cest-group/boss"
__description__ = "Bayesian optimization structure search"
__authors__ = [
    "Ville Parkkinen",
    "Henri Paulamaki",
    "Arttu Tolvanen",
    "Ulpu Remes",
    "Nuutti Sten",
    "Joakim Loefgren",
    "Milica Todorovic",
]
__maintainer__ = "The BOSS developers team"
__maintainer_email__ = "milica.todorovic@aalto.fi"
__license__ = "Apache License 2.0"
