import unittest
from unittest.mock import Mock

import numpy as np
from numpy.testing import assert_allclose, assert_equal

from boss.bo.acq.ei import ei
from boss.bo.acq.lcb import lcb
from boss.bo.acq.elcb import elcb
from boss.bo.acq.exploit import exploit
from boss.bo.acq.explore import explore


class AcqfnTest(unittest.TestCase):
    """
    Tests for acquisition functions
    """

    def setUp(self):
        """
        Initialization: Mock model predictions for acqfn use
        """
        self.model = Mock()
        self.model.X.shape = (10, 2)
        self.model.Y = np.array([[1]] * 10)
        self.model.predict_mean_sd_grads.return_value = (
            1,
            2,
            np.array([3, 4]),
            np.array([5, 6]),
        )
        self.model.predict.return_value = (1, 2)
        self.model.predict_grads.return_value = (
            np.array([[[3], [4]]]),
            np.array([[5, 6]]),
        )
        self.params = (1, 0)

    def test_lcb(self):

        val, grad = lcb(np.array([[0, 0]]), self.model, self.params)

        expected_val = -1
        expected_grad = np.array([[-2], [-2]])

        self.assertEqual(val, expected_val)
        assert_equal(grad, expected_grad)

    def test_elcb(self):

        val, grad = elcb(np.array([[0, 0]]), self.model, self.params)

        expected_val = -5.0114414
        expected_grad = np.array([[-12.0286035], [-14.0343243]])

        self.assertAlmostEqual(val, expected_val)
        assert_allclose(grad, expected_grad, atol=1e-7)

    def test_explore(self):
        val, grad = explore(np.array([[0, 0]]), self.model, self.params)

        expected_val = -2
        expected_grad = np.array([[-5], [-6]])

        self.assertEqual(val, expected_val)
        assert_equal(grad, expected_grad)

    def test_exploit(self):
        val, grad = exploit(np.array([[0, 0]]), self.model, self.params)

        expected_val = 1
        expected_grad = np.array([[3], [4]])

        self.assertEqual(val, expected_val)
        assert_equal(grad, expected_grad)

    def test_ei(self):
        val, grad = ei(np.array([[0, 0]]), self.model, self.params)

        expected_val = -0.5641896
        expected_grad = np.array([[0.7947630], [1.1537156]])

        self.assertAlmostEqual(val, expected_val)
        assert_allclose(grad, expected_grad, atol=1e-7)


if __name__ == "__main__":
    unittest.main()
