import unittest
from unittest.mock import Mock
import numpy as np

from boss.bo.kernel_factory import KernelFactory
from boss.settings import Settings

from boss.bo.kernel_factory import KernelFactory


class KernelFactoryTest(unittest.TestCase):
    def setUp(self):
        self.settings = Settings(dict(bounds=np.array([[0.0, 1.0]] * 4)))
        self.dim = self.settings.dim
        self.settings["thetainit"] = [9.0, 2.1, 2.2, 2.3, 2.4]
        self.settings["thetaprior"] = "gamma"
        self.settings["thetapriorpar"] = [[1, 1]] * (self.dim + 1)
        self.settings["kernel"] = ["stdp", "rbf", "mat32", "mat52"]
        self.settings["thetabounds"] = [(0, 10)] * (self.dim + 1)
        self.settings["periods"] = [4.0, None, None, None]

    def test_default(self):
        kern = KernelFactory.construct_kernel(self.settings)
        lengthscales = self.settings["thetainit"][1:]
        variances = [self.settings["thetainit"][0]] + [1.0] * (self.dim - 1)

        for i, k in enumerate(kern.parts):
            self.assertEqual(k.lengthscale[0], lengthscales[i])
            self.assertEqual(k.variance[0], variances[i])

    def test_forced_hypers(self):
        params = [0.1, 5.2, 6.4, 7.6, 8.8]
        kern = KernelFactory.construct_kernel(self.settings, forced_hypers=params)
        lengthscales = params[1:]
        variances = [params[0]] * self.dim
        for i, k in enumerate(kern.parts):
            self.assertEqual(k.lengthscale[0], lengthscales[i])
            self.assertEqual(k.variance[0], variances[i])

    # def test_grad_true(self):
    #     kern = KernelFactory.construct_kernel(self.settings, grad=True)


if __name__ == "__main__":
    unittest.main()
