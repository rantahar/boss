import numpy as np
import pytest
from pytest import approx
from pathlib import Path
from inspect import getfile

from gl_grad import f as userfn

import boss.keywords as bkw


def test_find_category():
    for key in [
        "ygrads",
        "pp_models",
        "pp_acq_funcs",
        "pp_truef_at_xhats",
        "initupdate",
        "ynorm",
    ]:
        assert bkw.find_category(key) == (bool, 0)

    for key in [
        "initpts",
        "verbosity",
        "iterpts",
        "updatefreq",
        "cores",
        "updaterestarts",
        "updateoffset",
        "mep_precision",
        "mep_rrtsteps",
        "mep_nebsteps",
        "pp_truef_npts",
        "hmciters",
        "minzacc",
    ]:
        assert bkw.find_category(key) == (int, 0)

    for key in ["pp_iters", "pp_model_slice"]:
        assert bkw.find_category(key) == (int, 1)

    for key in ["noise", "acqtol", "min_dist_acqs", "mep_maxe", "pp_local_minima"]:
        assert bkw.find_category(key) == (float, 0)

    for key in [
        "yrange",
        "gm_tol",
        "periods",
        "acqfnpars",
        "pp_var_defaults",
        "thetainit",
    ]:
        assert bkw.find_category(key) == (float, 1)

    for key in ["bounds", "thetapriorpar", "thetabounds"]:
        assert bkw.find_category(key) == (float, 2)

    for key in [
        "outfile",
        "ipfile",
        "rstfile",
        "acqfn_name",
        "inittype",
        "thetaprior",
        "userfn",
    ]:
        assert bkw.find_category(key) == (str, 0)

    for key in ["kernel"]:
        assert bkw.find_category(key) == (str, 1)


def test_eval_bool():
    for true_str in ["1", "True", "true", "T", "t", "Yes", "yes", "Y", "y"]:
        assert bkw._eval_bool(true_str) == True
    for false_str in ["0", "False", "false", "F", "f", "No", "no", "N", "n"]:
        assert bkw._eval_bool(false_str) == False


def test_destringify():
    # None
    assert bkw.destringify("None", ("foo", 42)) is None
    assert bkw.destringify("none", ("foo", 42)) is None

    # 0d types
    assert bkw.destringify("1", (int, 0)) == 1
    assert bkw.destringify("0", (int, 0)) == 0
    assert bkw.destringify("2.0", (float, 0)) == approx(2.0)
    assert bkw.destringify("foo", (str, 0)) == "foo"
    assert bkw.destringify("0", (bool, 0)) == False
    assert bkw.destringify("1", (bool, 0)) == True

    # 1d types
    assert bkw.destringify("foo bar", (str, 1)) == ["foo", "bar"]
    np.testing.assert_equal(bkw.destringify("5 6", (int, 1)), np.array([5, 6]))
    assert bkw.destringify("7.0 8.0", (float, 1)) == approx(np.array([7.0, 8.0]))

    # 2d types
    arr_str = "5.0 3.0; 2.0 1.0"
    arr_target = np.array([[5.0, 3.0], [2.0, 1.0]])
    assert bkw.destringify(arr_str, (float, 2)) == approx(arr_target)

    iarr_str = "4 3; 2 1"
    iarr_target = np.array([[4, 3], [2, 1]])
    np.testing.assert_equal(bkw.destringify(iarr_str, (int, 2)), iarr_target)


def test_func_from_keyword():
    # functions
    fpath = getfile(userfn)
    assert bkw.func_from_keyword(str(fpath)) == userfn
    assert bkw.func_from_keyword(f"{fpath} {userfn.__name__}") == userfn


def test_stringify():
    """Tests the conversion from Python types to boss string format. """
    # None
    assert bkw.stringify(None) == "None"

    # 0d types
    assert bkw.stringify(1) == "1"
    assert bkw.stringify(2.0) == "2.0"
    assert bkw.stringify(np.int64(3)) == "3"
    assert bkw.stringify(np.float64(4.0)) == "4.0"
    assert bkw.stringify("foo") == "foo"
    assert bkw.stringify(True) == "1"
    assert bkw.stringify(False) == "0"

    # 1d types
    assert bkw.stringify(["foo", "bar"]) == "foo bar"
    assert bkw.stringify([5, 6]) == "5 6"
    assert bkw.stringify([7.0, 8.0]) == "7.0 8.0"
    assert bkw.stringify(np.array([9, 10], dtype=np.int64)) == "9 10"
    assert bkw.stringify(np.array([11.0, 12.0], dtype=np.float64)) == "11.0 12.0"

    # 2d types
    arr = np.array([[3.0, 2.0], [1.0, 0.0]], dtype=np.float64)
    assert bkw.stringify(arr) == "3. 2.; 1. 0."
    iarr = np.array([[4, 3], [2, 1]], dtype=np.int64)
    assert bkw.stringify(iarr) == "4 3; 2 1"


def test_func_to_keyword():
    # functions
    true_path = getfile(userfn)
    assert bkw.func_to_keyword(userfn) == f"{true_path} f"
