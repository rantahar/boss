import numpy as np
from numpy.testing import assert_equal
import io
import unittest
from unittest.mock import mock_open, patch, Mock

import boss.io.parse as parse
from boss.settings import Settings

import shutil
from pathlib import Path
import pytest
from pytest import approx


@pytest.fixture(scope="session")
def ipfile(tmp_path_factory):
    """Temporary boss input file for testing. """

    tmp_file = tmp_path_factory.mktemp("scratch") / "boss.in"
    with open(tmp_file, "w") as fd:
        # fd.write("userfn       gl.py\n")
        fd.write("bounds       0.5 2.5\n")
        fd.write("kernel       rbf\n")
        fd.write("#kernel       rbf\n")
        fd.write("\n")
        fd.write("yrange       -1 6\n")
        fd.write("initpts   10 # comment\n")
        fd.write("iterpts 20 \n")

    return tmp_file


@pytest.fixture(scope="session")
def rstfile(tmp_path_factory):
    """Temporary boss rst file for testing. """

    tmp_file = tmp_path_factory.mktemp("scratch") / "boss.rst"
    with open(tmp_file, "w") as fd:
        # fd.write("userfn              user_functions/NDPeriodic.py\n")
        fd.write("bounds    -0.5 0.5; -0.5 0.5\n")
        fd.write("kernel    stdp\n")
        fd.write("yrange       -1 1\n")
        fd.write("initpts            3\n")
        fd.write("iterpts              4\n")
        fd.write("verbosity            2\n")
        fd.write("\n")
        fd.write("# a comment\n")
        fd.write("pp_truef_npts        30 # foo\n")
        fd.write("#pp_local_minima      30\n")
        fd.write("\n")
        fd.write("RESULTS:\n")
        fd.write("  0.0000000E+00  0.0000000E+00  7.0807342E-01\n")
        fd.write("  2.5000000E-01 -2.5000000E-01 -2.9192658E-01\n")
        fd.write(" -2.5000000E-01  2.5000000E-01 -2.9192658E-01\n")
        fd.write(
            " -4.3750000E-01  4.3750000E-01   5.6162681E-01   2.3259465E-01   3.9949275E-01  3.9949275E-01\n"
        )
        fd.write(
            " -4.7310345E-01  2.6782952E-02  -8.4680251E-01   2.3259465E-01   3.9949275E-01  3.9949275E-01\n"
        )
        fd.write(
            "  2.5853959E-02 -4.7403390E-01  -8.4257090E-01   2.3259465E-01   3.9949275E-01  3.9949275E-01\n"
        )
        fd.write(
            "  2.0515440E-02 -3.3097379E-01  -7.9705644E-01   2.3259465E-01   3.9949275E-01  3.9949275E-01\n"
        )
        fd.write("   \n")
    return tmp_file


@pytest.fixture(scope="session")
def rstfile_initpts(tmp_path_factory):
    """Temporary boss rst file for testing.

    Only contains initpts in the results section.
    """
    tmp_file = tmp_path_factory.mktemp("scratch") / "boss_only_inipts.rst"
    with open(tmp_file, "w") as fd:
        fd.write("bounds       0 1; 0 1\n")
        fd.write("\n")
        fd.write("RESULTS:\n")
        fd.write(
            "0.0000000000000000e+00   0.0000000000000000e+00   0.0000000000000000e+00\n"
        )
        fd.write(
            "0.0000000000000000e+00   1.0000000000000000e+00   1.0000000000000000e+00\n"
        )
        fd.write("1.0000000000000000e+00   0.0000000000000000e+00\n")
        fd.write(
            "# 1.0000000000000000e+00   1.0000000000000000e+00   2.0000000000000000e+00\n"
        )
        fd.write(
            "5.0000000000000000e-01   5.0000000000000000e-01   1.0000000000000000e+00 # foo \n"
        )
        fd.write("    \n")
    return tmp_file


def test_input_file(ipfile, rstfile, rstfile_initpts):

    #
    # == Reading an ordinary boss.in file. ==
    #
    data = parse.parse_input_file(ipfile)
    keywords = data["keywords"]
    assert data["is_rst"] == False
    # assert callable(keywords['userfn'])
    assert keywords["kernel"] == ["rbf"]
    assert keywords["bounds"] == approx(np.array([[0.5, 2.5]]))
    assert keywords["yrange"] == approx(np.array([-1.0, 6.0]))
    assert keywords["initpts"] == 10
    assert keywords["iterpts"] == 20

    #
    # == Reading an ordinary boss.in file. ==
    #
    data = parse.parse_input_file(rstfile)
    keywords = data["keywords"]
    assert data["is_rst"] == True
    # assert callable(keywords['userfn'])
    assert keywords["kernel"] == ["stdp"]
    assert keywords["bounds"] == approx(np.array([[-0.5, 0.5], [-0.5, 0.5]]))
    assert keywords["yrange"] == approx(np.array([-1.0, 1.0]))
    assert keywords["initpts"] == 3
    assert keywords["iterpts"] == 4
    assert keywords["verbosity"] == 2
    assert keywords["pp_truef_npts"] == 30
    rst_data = np.array(
        [
            [0.0000000e00, 0.0000000e00, 7.0807342e-01, np.nan, np.nan, np.nan],
            [2.5000000e-01, -2.5000000e-01, -2.9192658e-01, np.nan, np.nan, np.nan],
            [-2.5000000e-01, 2.5000000e-01, -2.9192658e-01, np.nan, np.nan, np.nan],
            [
                -4.3750000e-01,
                4.3750000e-01,
                5.6162681e-01,
                2.3259465e-01,
                3.9949275e-01,
                3.9949275e-01,
            ],
            [
                -4.7310345e-01,
                2.6782952e-02,
                -8.4680251e-01,
                2.3259465e-01,
                3.9949275e-01,
                3.9949275e-01,
            ],
            [
                2.5853959e-02,
                -4.7403390e-01,
                -8.4257090e-01,
                2.3259465e-01,
                3.9949275e-01,
                3.9949275e-01,
            ],
            [
                2.0515440e-02,
                -3.3097379e-01,
                -7.9705644e-01,
                2.3259465e-01,
                3.9949275e-01,
                3.9949275e-01,
            ],
        ]
    )
    np.testing.assert_almost_equal(data["rst_data"], rst_data)

    #
    # == Reading a boss.rst file with only initpts data in the results section. ==
    #
    data = parse.parse_input_file(rstfile_initpts)
    keywords = data["keywords"]
    assert data["is_rst"] == True
    assert keywords["bounds"] == approx(np.array([[0.0, 1.0], [0.0, 1.0]]))
    rst_data = np.array(
        [
            [0.0000000000000000e00, 0.0000000000000000e00, 0.0000000000000000e00],
            [0.0000000000000000e00, 1.0000000000000000e00, 1.0000000000000000e00],
            [1.0000000000000000e00, 0.0000000000000000e00, np.nan],
            [5.0000000000000000e-01, 5.0000000000000000e-01, 1.0000000000000000e00],
        ]
    )
    np.testing.assert_almost_equal(data["rst_data"], rst_data)


class ParseTest(unittest.TestCase):
    """
    Test cases for io.parse module
    """

    def setUp(self):
        self.settings = Settings({"bounds": np.array([[0.0, 1.0]] * 2)})
        self.dim = self.settings.dim
        # self.settings.batchsize = 3
        self.settings["initpts"] = 14
        self.settings["ygrads"] = False

    def test_acqs(self):
        ref_data = np.array(
            [[0.3, -2.0, 5.1], [3.5, 3.6, 6.3], [7.1, 3.9, -10], [26.0, -4.0, 7.1]]
        )
        test_string = (
            "\nsome\nData point added to dataset\n%.2f %.2f %.2f\n"
            "random\nData point added to dataset\n%.2e %.2e %.2e\n"
            "stuff \nData point added to dataset\n%.5f %.5f %.5f\n"
            "here\n\nData point added to dataset\n%.5e %.5e %.5e\n"
        ) % tuple(ref_data.flatten())

        with patch("boss.io.parse.open", mock_open(read_data=test_string)):
            data = parse.parse_acqs(self.settings, "foo")

        assert_equal(data[:, 0], np.arange(1, 5))
        assert_equal(data[:, 1:], ref_data)

    def test_min_preds(self):
        ref_data = np.array([[2, 3.4, 5.6], [5, 7.8, 2.3]])
        test_string = (
            "some headers\nthis shouldn't affect parsing\nBO "
            "Total ensemble size 2\nwhatever\nGlobal minimum prediction"
            "\n%8.3f %8.3f\nsomething\nelse\nTotal ensemble size 5"
            "\nGlobal minimum prediction\n%8.3e %8.3e"
            "\nrest of stuff"
        ) % tuple(ref_data[:, 1:].flatten())

        with patch("boss.io.parse.open", mock_open(read_data=test_string)):
            data = parse.parse_min_preds(self.settings, "foo")

        assert_equal(data, ref_data)


if __name__ == "__main__":
    unittest.main()
